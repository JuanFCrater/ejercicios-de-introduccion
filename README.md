# Ejercicion de Introduccion #

En este proyecto podemos ver 4 pequeñas aplicaciones que cumplen su propia funcion.

### Conversor Moneda ###

 Esta activity realiza el cambio de divisas de euros a dolares y viceversa
 Además nos permitira añadir el valor de cambio manualmente

### Contador Cafes ###

 Esta activity gestiona, mediante un contador, el tiempo que tomamos en un descanso para "un cafe", además contara los cafes tomados en una sesion.
 Cuando el contador llega a 0, la aplicacion reproduce un sonido y suma un cafe al contador.
 Si el contador llega a 10, no podra realizar mas sesiones y se monstrara un mensaje

### WebView ###

Esta formado por dos activities
	WebView: Esta activity pide una dirección web y envia esta a otra activity que la muestra en un webview
	WebViewShow: Esta es la activity que recoge la dirección web de la otra activity y la muestra en un webview en la interface

### Pomodoro ###
Esta aplicacion permite aplicar el metodo pomodoro a una sesion de trabajo/estudio
La Técnica Pomodoro es un método para mejorar la administración del tiempo desarrollado por Francesco Cirillo a fines de los años 1980.
La técnica usa un reloj para dividir el tiempo dedicado a un trabajo en intervalos de 25 minutos(pudiendo variarse) -llamados 'pomodoros' o 'sesiones'- separados por pausas.
Cada cuatro "pomodoros" tomar una pausa más larga (20 ó 30 minutos)

Esta formado por dos activities:
	Pomodoro: En esta activity podemos cambiar los tiempos e iniciar el tiempo
	SesionIniciada: En esta activity se muestra el estado de sesion y el tiempo

